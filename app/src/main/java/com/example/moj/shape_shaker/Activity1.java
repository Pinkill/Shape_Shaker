package com.example.moj.shape_shaker;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Locale;

public class Activity1 extends AppCompatActivity implements TextToSpeech.OnInitListener, AdapterView.OnItemSelectedListener, View.OnClickListener{

    TextToSpeech mTTS;
    EditText written;
    Spinner language;
    Button nextActivity, textToSpeech;
    RelativeLayout rel;
    protected TouchHandler.customViewGroup blockingView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Naredi fullscreen in zakrije notification bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Samo landscape možen način
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_1);

        //Inicializacija widget spremenljivk
        written = findViewById(R.id.editTextActivity1);
        language = findViewById(R.id.languageActivity1);
        nextActivity = findViewById(R.id.buttonActivity1);
        textToSpeech = findViewById(R.id.textToVoiceActivity1);
        rel = findViewById(R.id.RelativeActivity1);

        //Preverja za notification bar ali potrebuje dovoljenje
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (!Settings.canDrawOverlays(this)) {
                Toast.makeText(this, "Please give my app this permission!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                this.startActivityForResult(intent, TouchHandler.OVERLAY_PERMISSION_REQ_CODE);
            } else {
                TouchHandler.preventStatusBarExpansion(this);
            }
        }
        else { TouchHandler.preventStatusBarExpansion(this); }

        //Nastavljanje detekcije dotika za dvojni swipe in trojni swipe
        TouchHandler tHand = new TouchHandler(rel, this);

        //List možnih opcij v spinnerju
        String[] spinnerItems = {"Angleščina", "Nemščina", "Francoščina", "Italjanščina"};

        //Kreiranje adapterja za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, spinnerItems);
        language.setAdapter(spinnerAdapter);

        //Nastavljanje mTTS za preverjanje kasneje
        mTTS = new TextToSpeech(this, this);

        //Nastavimo, da preverja na klik gumbov in dropdowna
        nextActivity.setOnClickListener(this);
        textToSpeech.setOnClickListener(this);
        language.setOnItemSelectedListener(this);
    }

    //Override back press-a tako, da ni možen klik nazaj
    @Override
    public void onBackPressed() {}

    //Ko dobimo rezultat glede vklopa dodatnih nastavitev
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TouchHandler.OVERLAY_PERMISSION_REQ_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.canDrawOverlays(this)) {
                    Toast.makeText(this, "User can access system settings without this permission!", Toast.LENGTH_SHORT).show();
                }
                else
                { TouchHandler.preventStatusBarExpansion(this); }
            }
        }
    }

    //Pretvorba texta v zvok
    @Override
    public final void onInit(final int status) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(status == TextToSpeech.SUCCESS){
                    mTTS.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                        @Override
                        public void onDone(String utteranceId) {
                            textToSpeech.setEnabled(true);
                        }

                        @Override
                        public void onError(String utteranceId) {
                            Toast.makeText(getApplicationContext(), "Vaša naprava ne podpira tega!", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onStart(String utteranceId) {
                            textToSpeech.setEnabled(false);
                        }
                    });
                }
                else{
                    Toast.makeText(getApplicationContext(), "Predvajanje ni uspelo!", Toast.LENGTH_SHORT).show();
                }
            }
        }).start();
    }

    //Pregled dropdown izbire
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
        switch(adapterView.getItemAtPosition(pos).toString()){
            case "Angleščina" : mTTS.setLanguage(Locale.ENGLISH);  break;
            case "Nemščina" : mTTS.setLanguage(Locale.GERMAN); break;
            case "Francoščina" : mTTS.setLanguage(Locale.FRENCH); break;
            case "Italjanščina" : mTTS.setLanguage(Locale.ITALIAN); break;
            default : Toast.makeText(getApplicationContext(), "Vstavite jezik!", Toast.LENGTH_SHORT).show(); break;
        }
    }

    // V primeru nobenega izbora
    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        Toast.makeText(getApplicationContext(), "Vstavite jezik!", Toast.LENGTH_SHORT).show();
    }

    //Zaznavanje klika gumbov
    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.textToVoiceActivity1 : startSpeech(); break;
            case R.id.buttonActivity1 : nextActivity(); break;
            default : break;
        }
    }

    //Naslednji activity
    private void nextActivity(){
        Intent intent = new Intent(this, Activity2.class);
        startActivity(intent);
    }

    //Methoda za začetek govora
    private void startSpeech(){
        mTTS.speak(written.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
    }

    //Uničuje ostanke govora ali notification blockerja
    @Override
    protected void onDestroy() {
        if(mTTS != null){
            mTTS.stop();
            mTTS.shutdown();
        }
        if (blockingView!=null) {
            WindowManager manager = ((WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE));
            if (manager != null) {
                manager.removeView(blockingView);
            }
        }
        super.onDestroy();
    }
}
