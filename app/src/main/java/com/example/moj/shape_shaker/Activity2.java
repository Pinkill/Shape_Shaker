package com.example.moj.shape_shaker;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class Activity2 extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener, View.OnClickListener{

    ImageView circle, rectangle, triangle, chosenImage;
    SeekBar volume;
    Button confirm;
    TextView volumePercentage;
    int volumeNumber;
    RelativeLayout rel;
    protected TouchHandler.customViewGroup blockingView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Naredi fullscreen in zakrije notification bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Samo landscape možen način
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_2);

        //Inicializacija widget spremenljivk
        circle = findViewById(R.id.circleActivity2);
        rectangle = findViewById(R.id.rectangleActivity2);
        triangle = findViewById(R.id.triangleActivity2);
        volume = findViewById(R.id.volumeActivity2);
        confirm = findViewById(R.id.acceptingShapeActivity2);
        volumePercentage = findViewById(R.id.volumePercentageActivity2);
        rel = findViewById(R.id.RelativeActivity2);

        volumeNumber = volume.getProgress();

        String izpis = volumeNumber + " %";
        volumePercentage.setText(izpis);

        //Preverja za notification bar ali potrebuje dovoljenje
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (!Settings.canDrawOverlays(this)) {
                Toast.makeText(this, "Please give my app this permission!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                this.startActivityForResult(intent, TouchHandler.OVERLAY_PERMISSION_REQ_CODE);
            } else {
                TouchHandler.preventStatusBarExpansion(this);
            }
        }
        else {
            TouchHandler.preventStatusBarExpansion(this);
        }

        //Nastavljanje detekcije dotika za dvojni swipe in trojni swipe
        TouchHandler tHand = new TouchHandler(rel, this);

        //Nastavimo, da preverja na klik gumbov in seekbara
        volume.setOnSeekBarChangeListener(this);
        circle.setOnClickListener(this);
        rectangle.setOnClickListener(this);
        triangle.setOnClickListener(this);
        confirm.setOnClickListener(this);
    }

    //Override back press-a tako, da ni možen klik nazaj
    @Override
    public void onBackPressed() {}

    //Ko dobimo rezultat glede vklopa dodatnih nastavitev
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TouchHandler.OVERLAY_PERMISSION_REQ_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.canDrawOverlays(this)) {
                    Toast.makeText(this, "User can access system settings without this permission!", Toast.LENGTH_SHORT).show();
                }
                else
                { TouchHandler.preventStatusBarExpansion(this);
                }
            }
        }
    }

    //Seekbar, spreminjanje pozicije
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
        volumeNumber = progress;
        String izpis = volumeNumber + " %";
        volumePercentage.setText(izpis);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        String izpis = volumeNumber + " %";
        volumePercentage.setText(izpis);
    }

    //Zaznavanje klika gumbov
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.circleActivity2 : imageViewBackgroundChanger(circle, rectangle, triangle); break;
            case R.id.rectangleActivity2 : imageViewBackgroundChanger(rectangle, circle, triangle); break;
            case R.id.triangleActivity2 : imageViewBackgroundChanger(triangle, rectangle, circle); break;
            case R.id.acceptingShapeActivity2 :
                int id = getDrawableId(chosenImage);
                if(id != -1){ drawNextActivity(id); break; }
                else{
                    break;
                }
            default: break;
        }
    }

    //Menanje background-a glede na izbiro oblike
    private void imageViewBackgroundChanger(ImageView subject, ImageView turnoff1, ImageView turnoff2){
        final int sdk = android.os.Build.VERSION.SDK_INT;
        chosenImage = subject;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            subject.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.buttons_shapes));
            turnoff1.setBackgroundResource(0);
            turnoff2.setBackgroundResource(0);
        } else {
            subject.setBackground(ContextCompat.getDrawable(this, R.drawable.buttons_shapes));
            turnoff1.setBackgroundResource(0);
            turnoff2.setBackgroundResource(0);
        }
    }

    //Pridobivanje Drawable Id-ja
    private int getDrawableId(ImageView subject){
        if(subject == null){
            Toast.makeText(getApplicationContext(), "Izberi lik!", Toast.LENGTH_SHORT).show();
            return -1;
        }
        else {
            if (subject.getDrawable() == circle.getDrawable()) {
                return R.drawable.circle;
            } else if (subject.getDrawable() == rectangle.getDrawable()) {
                return R.drawable.rectangle;
            } else if (subject.getDrawable() == triangle.getDrawable()) {
                return R.drawable.triangle;
            } else {
                Toast.makeText(getApplicationContext(), "Ne najdem lika!", Toast.LENGTH_SHORT).show();
                return -1;
            }
        }
    }

    //Naprej v risanje oblike
    private void drawNextActivity(int imageId){
        Intent intent = new Intent(getApplicationContext(), DrawActivity2.class);
        intent.putExtra("Volume", volumeNumber);
        intent.putExtra("Shape", imageId);
        startActivity(intent);

    }

    //Uničuje ostanke notification blockerja
    @Override
    protected void onDestroy() {
        if (blockingView!=null) {
            WindowManager manager = ((WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE));
            if (manager != null) {
                manager.removeView(blockingView);
            }
        }
        super.onDestroy();
    }
}
