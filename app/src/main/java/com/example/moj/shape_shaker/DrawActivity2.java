package com.example.moj.shape_shaker;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class DrawActivity2 extends AppCompatActivity implements View.OnTouchListener{

    int imageId, volumeNumber, width, height;
    float shapewidth, shapeheight;
    ImageView shape;
    Bitmap bitmap;
    RelativeLayout rel;
    private int GLOBAL_TOUCH_POSITION_X = 0;
    private int GLOBAL_TOUCH_POSITION_Y = 0;
    protected TouchHandler.customViewGroup blockingView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Naredi fullscreen in zakrije notification bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Samo landscape možen način
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_draw2);

        //Dobimo poslane podatke
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            volumeNumber = bundle.getInt("Volume");
            imageId = bundle.getInt("Shape");
        }

        //Preverja za notification bar ali potrebuje dovoljenje
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (!Settings.canDrawOverlays(this)) {
                Toast.makeText(this, "Please give my app this permission!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                this.startActivityForResult(intent, TouchHandler.OVERLAY_PERMISSION_REQ_CODE);
            } else {
                TouchHandler.preventStatusBarExpansion(this);
            }
        }
        else {
            TouchHandler.preventStatusBarExpansion(this);
        }

        //Inicializacija widget spremenljivk
        shape = findViewById(R.id.shapeDrawActivity2);
        rel = findViewById(R.id.Rel1DrawActivity2);

        //Izmerimo ekran
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        shapeheight = height*((float)volumeNumber/100);
        shapewidth = width*((float)volumeNumber/100);

        //Kličemo metodo za oblikovanje lika
        drawShape();
        bitmap = getBitmapFromVectorDrawable(this, imageId);

        //Nastavimo zaznavanje dotika na celotnem ekranu, root layout
        rel.setOnTouchListener(this);
    }

    //Override back press-a tako, da ni možen klik nazaj
    @Override
    public void onBackPressed() {}



    //Ko dobimo rezultat glede vklopa dodatnih nastavitev
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TouchHandler.OVERLAY_PERMISSION_REQ_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.canDrawOverlays(this)) {
                    Toast.makeText(this, "User can access system settings without this permission!", Toast.LENGTH_SHORT).show();
                }
                else
                { TouchHandler.preventStatusBarExpansion(this);
                }
            }
        }
    }

    //Narišemo lik
    void drawShape(){
        if(imageId != 0 || volumeNumber != 0){
            shape.getLayoutParams().height = (int)shapeheight;
            shape.getLayoutParams().width = (int)shapewidth;
        }
    }

    //Ob dotiku ekrana se sproži ta funkcija
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int x = (int)motionEvent.getX();
        int y = (int)motionEvent.getY();
        int pointerCount = motionEvent.getPointerCount();

        //en prst
        if(pointerCount == 1){
            if(x>0 && y>0 && x<width && y<height){
                float pixel = bitmap.getPixel(x,y);
                switch(motionEvent.getAction()){
                    case MotionEvent.ACTION_MOVE :
                        if(pixel == getResources().getColor(R.color.colorPrimary)){
                            Vibrator vb = (Vibrator)   getSystemService(Context.VIBRATOR_SERVICE);
                            if (Build.VERSION.SDK_INT >= 26) {
                                if (vb != null) {
                                    vb.vibrate(VibrationEffect.createOneShot(100,10));
                                }
                            } else {
                                if (vb != null) {
                                    vb.vibrate(100);
                                }
                            }
                        }
                        else{ break; }
                        break;
                    case MotionEvent.ACTION_UP :
                    case MotionEvent.ACTION_CANCEL :
                    case MotionEvent.ACTION_OUTSIDE : break;
                }
            }
            pointerCount = 0;
            return true;
        }
        //dva prsta
        final int SWIPE_THRESHOLD = 150;
        int GLOBAL_TOUCH_CURRENT_POSITION_X;
        int GLOBAL_TOUCH_CURRENT_POSITION_Y;
        if (pointerCount == 2) {
            int action = motionEvent.getActionMasked();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    GLOBAL_TOUCH_POSITION_X = (int) motionEvent.getX(1);
                    break;
                case MotionEvent.ACTION_UP:
                    GLOBAL_TOUCH_CURRENT_POSITION_X = 0;
                    break;
                case MotionEvent.ACTION_MOVE:
                    GLOBAL_TOUCH_CURRENT_POSITION_X = (int) motionEvent.getX(1);
                    int diff = GLOBAL_TOUCH_POSITION_X - GLOBAL_TOUCH_CURRENT_POSITION_X;
                    if(diff > SWIPE_THRESHOLD){
                        finish();
                    }
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    GLOBAL_TOUCH_POSITION_X = (int) motionEvent.getX(1);
                    break;
                default: break;
            }
            pointerCount = 0;
            return true;
        }
        //trije prsti
        else if(pointerCount == 3){
            int action = motionEvent.getActionMasked();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    GLOBAL_TOUCH_POSITION_Y = (int) motionEvent.getY(2);
                    break;
                case MotionEvent.ACTION_UP:
                    GLOBAL_TOUCH_CURRENT_POSITION_Y = 0;
                    break;
                case MotionEvent.ACTION_MOVE:
                    GLOBAL_TOUCH_CURRENT_POSITION_Y = (int) motionEvent.getY(2);
                    int diff = GLOBAL_TOUCH_POSITION_Y - GLOBAL_TOUCH_CURRENT_POSITION_Y;
                    if(diff < -SWIPE_THRESHOLD){
                        System.exit(0);
                    }
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    GLOBAL_TOUCH_POSITION_Y = (int) motionEvent.getY(2);
                    break;
                default: break;
            }
            pointerCount = 0;
            return true;
        }
        else {
            GLOBAL_TOUCH_POSITION_X = 0;
            GLOBAL_TOUCH_CURRENT_POSITION_X = 0;
            GLOBAL_TOUCH_POSITION_Y = 0;
            GLOBAL_TOUCH_CURRENT_POSITION_Y = 0;
            return  true;
        }
    }

    //Izrišemo lik in dobimo bitmap
    private Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
            Drawable drawable = (DrawableCompat.wrap(ContextCompat.getDrawable(context, drawableId))).mutate();
            try {
                Bitmap bitmap = Bitmap.createBitmap(width,
                        height, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                if(width>height) {
                    drawable.setBounds((width - (int) shapeheight) / 2, (height - (int) shapeheight) / 2, (int) shapeheight + (width - (int) shapeheight) / 2, (int) shapeheight + (height - (int) shapeheight) / 2);
                }
                else{
                    drawable.setBounds((width - (int) shapewidth) / 2, (height - (int) shapewidth) / 2, (int) shapewidth + (width - (int) shapewidth) / 2, (int) shapewidth + (height - (int) shapewidth) / 2);
                }
                drawable.draw(canvas);
                shape.setImageDrawable(drawable);
                return bitmap;
            } catch (OutOfMemoryError e) {
                return null;
            }
        }
        else {
            Drawable drawable = ContextCompat.getDrawable(context, drawableId);
            VectorDrawable vectorDrawable =  (VectorDrawable) drawable;
            Bitmap bitmap = Bitmap.createBitmap(width,
                    height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            if(width>height) {
                vectorDrawable.setBounds((width - (int) shapeheight) / 2, (height - (int) shapeheight) / 2, (int) shapeheight + (width - (int) shapeheight) / 2, (int) shapeheight + (height - (int) shapeheight) / 2);
            }
            else{
                vectorDrawable.setBounds((width - (int) shapewidth) / 2, (height - (int) shapewidth) / 2, (int) shapewidth + (width - (int) shapewidth) / 2, (int) shapewidth + (height - (int) shapewidth) / 2);
            }
            vectorDrawable.draw(canvas);
            shape.setImageDrawable(vectorDrawable);

            return bitmap;
        }
    }

    //Uničuje ostanke notification blockerja
    @Override
    protected void onDestroy() {
        if (blockingView!=null) {
            WindowManager manager = ((WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE));
            if (manager != null) {
                manager.removeView(blockingView);
            }
        }
        super.onDestroy();
    }
}
