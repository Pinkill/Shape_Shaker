package com.example.moj.shape_shaker;

import android.app.Activity;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Build;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;


public class TouchHandler implements View.OnTouchListener{

    private int GLOBAL_TOUCH_POSITION_X = 0;
    private int GLOBAL_TOUCH_POSITION_Y = 0;
    private Context context;
    static final int OVERLAY_PERMISSION_REQ_CODE = 4545;

    //Konstruktor
    TouchHandler(RelativeLayout rel,Context context){
        touchListener(rel);
        this.context = context;
    }

    //Nastavimo zaznavanje dotika na celotnem ekranu, root layout
    private void touchListener(RelativeLayout rel){
        rel.setOnTouchListener(this);
    }

    //Poseben viewGroup za notification bar
    public static class customViewGroup extends ViewGroup {

        public customViewGroup(Context context) {
            super(context);
        }

        @Override
        protected void onLayout(boolean changed, int l, int t, int r, int b) {
        }

        @Override
        public boolean onInterceptTouchEvent(MotionEvent ev) {
            return true;
        }
    }

    //Ustvarjanje okna na zgornjem delu ekrana za zajem dotika (za notification)
    static void preventStatusBarExpansion(Context context) {
        WindowManager manager = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE));

        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            localLayoutParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        }
        else{
            localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        }
        localLayoutParams.gravity = Gravity.TOP;
        localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|

            WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
            WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

        localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        localLayoutParams.height = (int) (20* context.getResources().getDisplayMetrics().scaledDensity);
        localLayoutParams.format = PixelFormat.TRANSPARENT;

        customViewGroup view = new customViewGroup(context);

        if (manager != null) {
            manager.addView(view, localLayoutParams);
        }
    }

    //Ob dotiku se sproži ta metoda
    @Override
    public boolean onTouch(View view, MotionEvent m) {
        final int SWIPE_THRESHOLD = 150;
        int pointerCount = m.getPointerCount();

        //Dva prsta
        int GLOBAL_TOUCH_CURRENT_POSITION_Y ;
        int GLOBAL_TOUCH_CURRENT_POSITION_X ;
        if (pointerCount == 2) {
            int action = m.getActionMasked();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    GLOBAL_TOUCH_POSITION_X = (int) m.getX(1);
                    break;
                case MotionEvent.ACTION_UP:
                    GLOBAL_TOUCH_CURRENT_POSITION_X = 0;
                    break;
                case MotionEvent.ACTION_MOVE:
                    GLOBAL_TOUCH_CURRENT_POSITION_X = (int) m.getX(1);
                    int diff = GLOBAL_TOUCH_POSITION_X - GLOBAL_TOUCH_CURRENT_POSITION_X;
                    if(diff > SWIPE_THRESHOLD){
                        ((Activity)context).finish();
                    }
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    GLOBAL_TOUCH_POSITION_X = (int) m.getX(1);
                    break;
                default:
            }
            pointerCount = 0;
            return true;
        }

        //Trije prsti
        else if(pointerCount == 3){
            int action = m.getActionMasked();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    GLOBAL_TOUCH_POSITION_Y = (int) m.getY(2);
                    break;
                case MotionEvent.ACTION_UP:
                    GLOBAL_TOUCH_CURRENT_POSITION_Y = 0;
                    break;
                case MotionEvent.ACTION_MOVE:
                    GLOBAL_TOUCH_CURRENT_POSITION_Y = (int) m.getY(2);
                    int diff = GLOBAL_TOUCH_POSITION_Y - GLOBAL_TOUCH_CURRENT_POSITION_Y;
                    if(diff < -SWIPE_THRESHOLD){
                        System.exit(0);
                    }
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    GLOBAL_TOUCH_POSITION_Y = (int) m.getY(2);
                    break;
                default: break;
            }
            pointerCount = 0;
            return true;
        }
        else {
            GLOBAL_TOUCH_POSITION_X = 0;
            GLOBAL_TOUCH_CURRENT_POSITION_X = 0;
            GLOBAL_TOUCH_POSITION_Y = 0;
            GLOBAL_TOUCH_CURRENT_POSITION_Y = 0;
            return  true;
        }
    }
}
